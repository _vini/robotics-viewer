#ifndef ITAS2D_WINDOW_SHADER_H
#define ITAS2D_WINDOW_SHADER_H

#include <GL/glew.h>
#include <string>
#include <iostream>


// Class for abstracting the usage of OpenGL shaders
// This is pretty much just a wrapper that makes it easier to use a shader
// In GLSL, you can have uniforms and attributes
// TODO: Explain a bit better why this class exists and how it might be useful
class Shader {
public:
    // The shader program ID
    GLuint pID;

    // vShaderCode: vertex shader source code
    // fShaderCode: fragment shader source code
    Shader(const std::string &vShaderCode, const std::string &fShaderCode);

    // Uniform setting functions
    void setBool(const std::string &name, bool value) const;

    void setInt(const std::string &name, int value) const;

    void setFloat(const std::string &name, float value) const;

    // TODO: Matrix uniforms. It's likely I'll be using glm

    void use() const;

    ~Shader() {
        // Safely delete shader from GPU memory
        // Check out Khronos' documentation for more info:
        // https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glDeleteProgram.xhtml
        glDeleteProgram(pID);
    }
};


#endif //ITAS2D_WINDOW_SHADER_H
