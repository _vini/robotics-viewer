# Build status
![](https://img.shields.io/gitlab/pipeline/_vini/robotics-viewer/main)

# How to build & run

Debug version:
```shell script
mkdir build
cd build
conan install .. -s build_type=Debug
cmake .. -G "Unix Makefiles"
cmake --build . --config Debug
cd bin
./itas2d-window
```

To build the Release version you may just replace all instances of "Debug" with "Release"(without quotes)

# Development

## Libraries

**Dear ImGui**: 

Immediate mode GUI library. This is the backbone of our application. Therefore, you should get *very* familiar using it.

If you are new to Dear ImGui, read the documentation from the docs/ folder + read the top of `imgui.cpp`.
Read online: https://github.com/ocornut/imgui/tree/master/docs

Also check out the example for OpenGL 3:
https://github.com/ocornut/imgui/tree/master/examples/example_glfw_opengl3

**GLFW**: 

[GLFW](https://www.glfw.org/) is a cross-platform general purpose library for handling windows, inputs, 
OpenGL/Vulkan/Metal graphics context creation, etc.

In this project, this library is used extensively, as OpenGL is at the core of our rendering backend.

**GLEW**:

[GLEW](http://glew.sourceforge.net/) is a helper library for exposing the OpenGL API as platform-agnostic functionality.

(Copied shamelessly from dear imgui's opengl+glfw+glew example):

Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.