#ifndef ITAS2D_WINDOW_RENDERTEXTURE_H
#define ITAS2D_WINDOW_RENDERTEXTURE_H

#include <GL/glew.h>

// This class implements a texture to which you can render
// your primitives
class RenderTexture {
private:
    GLuint FBO = 0;
    GLuint texColorBuffer = 0;
    size_t width, height;
public:
    RenderTexture(size_t width, size_t height);

    // Begin rendering
    void begin() const;
    // Finish rendering
    void end() const;

    GLuint get_texture() const {
        return texColorBuffer;
    }

    ~RenderTexture();
};


#endif //ITAS2D_WINDOW_RENDERTEXTURE_H
