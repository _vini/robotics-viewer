#include <cmath>
#include "CircleGeometry.h"

// FIXME: "Clang-Tidy: Initialization of 'vShaderSource'
//  with static storage duration may throw an exception that cannot be caught"
const std::string CircleGeometry::vShaderSource =
        "#version 330 core\n"
        "layout (location = 0) in vec3 inPos;\n"
        "void main() {\n"
        " gl_Position = vec4(inPos, 1.0);\n"
        "}\n";

const std::string CircleGeometry::fShaderSource =
        "#version 330 core\n"
        "out vec4 fragColor;\n"
        "void main() {\n"
        " fragColor = vec4(0.5);\n"
        "}\n";

void CircleGeometry::init() {
    // Initialize vertices array with unit circle
    for(int i=0; i < NUM_VERTICES(RESOLUTION); i++) {
        constexpr float theta = 2 * M_PI / NUM_VERTICES(RESOLUTION);
        vertices[i*3] = 0.5f*cosf(i*theta);
        vertices[i*3+1] = 0.5f*sinf(i*theta);
        vertices[i*3+2] = 0.0;
    }
    // Creating the VAO in GPU memory
    glGenVertexArrays(1, &VAO);
    // Creating the VBO in GPU memory
    glGenBuffers(1, &VBO);
    // bind the Vertex Array Object first, then bind and set the vertex buffer, and then configure vertex attribute(s).
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);
    // TODO:Maybe a color vertex attribute?
}

void CircleGeometry::draw() const {
    shaderProgram.use();
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLE_FAN, 0, NUM_VERTICES(RESOLUTION));
}

CircleGeometry::~CircleGeometry() {
    // De-allocate OpenGL objects
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}
