#include "RenderTexture.h"
#include <iostream>

RenderTexture::RenderTexture(size_t width, size_t height) : width(width), height(height){
    // Create the Framebuffer Object
    glGenFramebuffers(1, &FBO);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);

    // Create the texture
    glGenTextures(1, &texColorBuffer);
    glBindTexture(GL_TEXTURE_2D, texColorBuffer);
    // Generate the texture with and RGBA color format
    // The last argument is nullptr because we create the texture with no data whatsoever
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    // Linear filters for the texture
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    // Attach the texture to the currently bound framebuffer object
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer, 0);


    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cerr << "ERROR:: Framebuffer is not complete!" << std::endl;
    // Unbind the newly created framebuffer
    // This binds back to the default(i.e. the window's) framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderTexture::begin() const {
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    // TODO: Maybe stop with the hardcoded color?
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    // Set the viewport dimensions
    glViewport(0, 0, width, height);
}

void RenderTexture::end() const {
    // Unbind the newly created framebuffer
    // This binds back to the default(i.e. the window's) framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

RenderTexture::~RenderTexture() {
    glDeleteFramebuffers(1, &FBO);
}


