#include "Shader.h"

Shader::Shader(const std::string &vShaderCode, const std::string &fShaderCode) {
    // Convert to c-strings
    const GLchar *vShaderCode_CStr = vShaderCode.c_str();
    const GLchar *fShaderCode_CStr = fShaderCode.c_str();

    // Compile shaders
    constexpr size_t infoSize = 512;
    GLuint vertex, fragment;
    int success;
    char infoLog[infoSize];

    // Vertex Shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode_CStr, nullptr);
    glCompileShader(vertex);
    // print compile errors if any
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex, infoSize, nullptr, infoLog);
        std::cerr << "ERROR: Vertex shader compilation failed!\n" << infoLog << std::endl;
    }

    // Similar for Fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode_CStr, nullptr);
    glCompileShader(fragment);
    // Print compile errors if any
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex, infoSize, nullptr, infoLog);
        std::cerr << "ERROR: Fragment shader compilation failed!\n" << infoLog << std::endl;
    }

    // Shader Program
    pID = glCreateProgram();
    glAttachShader(pID, vertex);
    glAttachShader(pID, fragment);
    glLinkProgram(pID);
    // print linking errors if any
    glGetProgramiv(pID, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(pID, infoSize, nullptr, infoLog);
        std::cerr << "ERROR: Shader program linking failed!\n" << infoLog << std::endl;
    }

    // Delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

void Shader::use() const {
    glUseProgram(pID);
}

void Shader::setBool(const std::string &name, bool value) const {
    glUniform1i(glGetUniformLocation(pID, name.c_str()), (int) value);
}

void Shader::setInt(const std::string &name, int value) const {
    glUniform1i(glGetUniformLocation(pID, name.c_str()), value);
}

void Shader::setFloat(const std::string &name, float value) const {
    glUniform1f(glGetUniformLocation(pID, name.c_str()), value);
}