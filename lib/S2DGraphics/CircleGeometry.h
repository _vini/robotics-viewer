#ifndef ITAS2D_WINDOW_CIRCLEGEOMETRY_H
#define ITAS2D_WINDOW_CIRCLEGEOMETRY_H


#define NUM_SIDES(high_res) (high_res ? 70 : 32)
#define NUM_VERTICES(high_res) (NUM_SIDES(high_res) + 2)
constexpr bool RESOLUTION = false;

#include "GL/glew.h"
#include "Shader.h"
#include <string>

class CircleGeometry {
private:
    // Vertex Buffer Object and Vertex Array Object IDs
    GLuint VBO, VAO;
    // This is going to be copied into GPU memory as a VBO for OpenGL
    GLfloat vertices[3 * NUM_VERTICES(RESOLUTION)]{};
    Shader shaderProgram;

    static const std::string vShaderSource;
    static const std::string fShaderSource;

    void init();

public:
    CircleGeometry() : shaderProgram(vShaderSource, fShaderSource) {
        VBO = VAO = 0;
        init();
    }

    void draw() const;

    ~CircleGeometry();
};


#endif //ITAS2D_WINDOW_CIRCLEGEOMETRY_H
